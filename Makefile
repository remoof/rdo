all: rdo.c
	gcc -D_FORTIFY_SOURCE=2 -O2 -s -Wl,-z,now -lcrypt -lbsd -Wall -Wextra -Werror rdo.c -o rdo

debug: rdo.c
	gcc -O0 -g -Wl,-z,now -lcrypt -lbsd -Wall -Wextra -Werror rdo.c -o rdo

install: rdo
	cp rdo /usr/bin/rdo
	chown root:root /usr/bin/rdo
	chmod 755 /usr/bin/rdo
	chmod u+s /usr/bin/rdo
	cp rdo_sample.conf /etc/rdo.conf

uninstall:
	rm /usr/bin/rdo
	rm /etc/rdo.conf

clean:
	rm rdo
