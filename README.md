# RootDO [![AUR](https://img.shields.io/aur/version/rdo.svg)](https://aur.archlinux.org/packages/rdo/)

This little "project" aims to be a very slim alternative to both sudo and doas.

### Installation

If you are on Arch Linux, you can download the package via the [AUR](https://aur.archlinux.org/packages/rdo/).

If you are using any other linux distro, you can build it yourself by following these instructions:

```sh
git clone https://codeberg.org/sw1tchbl4d3/rdo
cd rdo
make
sudo make install
```

After that, you'll have to configure rdo to allow you to use it.
To do this, edit `/etc/rdo.conf`, and set the username variable to your own.

After that you're good to go!

And to uninstall:
```sh
sudo make uninstall
```

### Usage

```sh
rdo [command]
```

The configuration file has the following variables:
```
username=sw1tchbl4d3
wrong_pw_sleep=1000
session_ttl=5
```

- `username`: The username of the user that is allowed to execute rdo (no multi user or group support (yet)).
- `wrong_pw_sleep`: The amount of milliseconds to sleep at a wrong password attempt. Must be a positive integer. Set to 0 to disable.
- `session_ttl`: The amount of minutes a session lasts. Must be a positive integer. Set to 0 to disable.

### Dependencies
- `libbsd`
